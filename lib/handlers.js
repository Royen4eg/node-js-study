let _data = require('./data');
let helpers = require('./helpers');

let handlers = {};

handlers.user = function (data, callback) {
    const acceptableMethods = ['post', 'get', 'put', 'delete'];

    if (acceptableMethods.indexOf(data.method) > -1) {
        handlers._user[data.method](data, callback);
    } else {
        callback(405);
    }
};

handlers._user = {};

handlers._user.post = function (data, callback) {
    const name = typeof (data.payload.name) == 'string' && data.payload.name.trim().length > 0 ? data.payload.name.trim() : false;
    const country = typeof (data.payload.country) == 'string' && data.payload.country.trim().length > 0 ? data.payload.country.trim() : false;
    const phone = typeof (data.payload.phone) == 'string' && data.payload.phone.trim().length === 10 ? data.payload.phone.trim() : false;
    const password = typeof (data.payload.password) == 'string' && data.payload.password.trim().length === 10 ? data.payload.password.trim() : false;
    const tosAgreement = typeof (data.payload.tosAgreement) == 'boolean' && data.payload.tosAgreement === true;

    if (name && country && phone && password && tosAgreement) {
        _data.read('users', name, function (err, data) {
            if (err) {
                const data = {
                    'name': name,
                    'country': country,
                    'phone': phone,
                    'hashedPassword': helpers.hashPassword(password),
                    'tosAgreement': tosAgreement
                };
                if (data.hashedPassword) {
                    _data.create('users', name, data, function (err) {
                        if (err) {
                            console.log(err);
                            callback(500, {'Error': 'Could not create the user'});
                        } else {
                            callback(500);
                        }
                    })
                } else {
                    callback(400, {'Error': 'Invalid password hash'})
                }
            } else {
                callback(400, {'Error': 'User with that name already exists'});
            }
        })
    } else {
        callback(400, {'Error': 'Missing required fields'});
    }
};

handlers._user.get = function (data, callback) {

};

handlers._user.put = function (data, callback) {

};

handlers._user.delete = function (data, callback) {

};

handlers.hello = function (data, callback) {
    callback(200, {'Message': 'anything you want'}, {'Content-Type': 'application/json'});
};

handlers.notFound = function (data, callback) {
    callback(404)
};

module.exports = handlers;