let crypto = require('crypto');
let config = require('../config');

let helpers = {};

helpers.hashPassword = function (p) {
    if (typeof (str) === 'string' && str.length > 0) {
        return crypto.createHmac('sha256', config.hashingSecret).update(p).digest('hex');
    } else {
        return false;
    }
};

helpers.parseJsonToObject = function (str) {
    try {
        return JSON.parse(str);
    }catch(e){
        return {};
    }
};


module.exports = helpers;