/*
 * Library storing
 */

let fs = require('fs');
var path = require('path');

// Module container
var lib = {};

lib.baseDir = path.join(__dirname, '/../.data/');
//write data
lib.create = function (dir, key, data, callback) {

    if (dir && !fs.existsSync(lib.baseDir + dir)) {
        fs.mkdir(lib.baseDir + dir, {recursive: true}, (err) => {
            if (err) throw err;
        })
    }
    //open file for writing
    fs.open(lib.baseDir + dir + '/' + key + '.json', 'wx', function (err, fD) {
        if (!err && fD) {
            const stringData = JSON.stringify(data);

            lib.writeFile(fD, stringData, callback);
        } else {
            callback('Could not create new file, it may already exist')
        }
    });
};

lib.read = function (dir, key, callback) {
    fs.readFile(lib.baseDir + dir + '/' + key + '.json', 'utf8', function (err, data) {
        callback(err, data);
    })
};

lib.update = function(dir, key, data, callback){
    fs.open(lib.baseDir + dir + '/' + key + '.json', 'r+', function (err, fD) {
        if (!err && fD) {
            const stringData = JSON.stringify(data);
            fs.ftruncate(fD, function (err) {
                if(!err){
                    lib.writeFile(fD, stringData, callback);
                } else {
                    callback('Error truncating the file');
                }
            });
        } else {
            callback('Could not update file. Creating file');
            lib.create(dir, key, data, callback);
        }
    });
};

lib.delete = function(dir, key, callback) {
    fs.unlink(lib.baseDir + dir + '/' + key + '.json', function (err) {
        if(!err){
            callback(false)
        } else {
            callback('Error deleting the file')
        }
    })
};

lib.writeFile = function(fD, stringData, callback){
    fs.writeFile(fD, stringData, function (err) {
        if (!err) {
            fs.close(fD, function (err) {
                if (!err) {
                    callback(false);
                } else {
                    callback('Error closing file!')
                }
            })
        } else {
            callback('Error writing to file!')
        }
    })
};

module.exports = lib;