/*
 * Create and export configuration as global variables
 */

let env = {};

env.staging = {
    "httpPort": 3000,
    "httpsPort": 3001,
    "envName": 'staging',
    "hashingSecret": 'secretKey'
};

env.production = {
    "httpPort": 5000,
    "httpsPort": 5001,
    "envName": 'prod',
    "hashingSecret": 'randomString'
};

const curEnv = typeof (process.env.NODE_ENV) === 'string' ? process.env.NODE_ENV.toLowerCase() : 'staging';

let envExp = typeof (env[curEnv]) === 'object' ? env[curEnv] : env.staging;

module.exports = envExp;