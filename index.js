'use strict';

// Dependencies
let http = require('http');
let https = require('https');
let url = require('url');
let StringDecoder = require('string_decoder').StringDecoder;
let config = require('./config');
let fs = require('fs');
let handlers = require('./lib/handlers');
let helpers = require('./lib/helpers');

//Create server configuration
let httpServer = http.createServer(function (req, res) {
    unifyServer(req, res)
});
//Start server and listener
httpServer.listen(config.httpPort, function () {
    console.log("I'm listening port number " + config.httpPort);
});

if (fs.existsSync('./https/host.key') && fs.existsSync('./https/host.cert')) {
    let httpsServer = https.createServer({
        'key': fs.readFileSync('./https/host.key'),
        'cert': fs.readFileSync('./https/host.cert')
    }, function (req, res) {
        unifyServer(req, res)
    });

    httpsServer.listen(config.httpsPort, function () {
        console.log("I'm listening port number " + config.httpsPort);
    });
} else {
    console.log('Could not start https server: Key or/and cert files are missing');
}

let unifyServer = function (req, res) {

    let parsedUrl = url.parse(req.url, true);

    // get http method (POST, GET, OPTION, ...)
    // (make sure you have lower OR upper case settled cause they might come in different formats)
    const method = req.method.toLocaleLowerCase();
    //path
    const path = parsedUrl.pathname;
    const trimmedPath = path.replace(/^\/+|\/+$/g, '');

    // object of params
    const queryParams = parsedUrl.query;

    //get headers as object
    let headers = req.headers;

    let decoder = new StringDecoder('utf-8');
    let buffer = '';
    //  req.on 'data' react on data streams (example is simple: POST). Any POST request will be carry data
    req.on('data', function (data) {
        buffer += decoder.write(data);
    });

    req.on('end', function () {
        buffer += decoder.end();

        // choose the direction of request
        let chosenHandler = typeof (router[trimmedPath]) !== 'undefined' ? router[trimmedPath] : handler.notFound;

        let data = {
            'reqPath': trimmedPath,
            'method ': method,
            'query ': queryParams,
            'headers ': headers,
            'payload ': helpers.parseJsonToObject(buffer),
        };

        // route to handler
        chosenHandler(data, function (statusCode, payload, headers) {
            statusCode = typeof (statusCode) === 'number' ? statusCode : 200;
            payload = typeof (payload) === 'object' ? payload : {};

            if (typeof (headers) === 'object') {
                for (let index in headers) {
                    if (headers.hasOwnProperty(index)) {
                        res.setHeader(index, headers[index]);
                    }
                }
            }

            const out = JSON.stringify(payload, null, 4);

            res.writeHead(statusCode);

            res.end(out);

            console.log('Request: ' + trimmedPath + '; ' + statusCode);
        });
    });
};


const router = {
    'user': handlers.user,
    'hello': handlers.hello
};